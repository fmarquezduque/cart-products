import React from 'react';
import Product from './Product';


function Products( { products, add } ) {

    return (

      <div className="container pt-5">
        <h2 className = "text-center" >Products</h2>
        <div className="row">
          {
            products.map((product) => (
              <Product key = { product.id } product = { product } add = { add }  />
            ))
          }
        </div>
      </div>

    )

}

export default Products;
