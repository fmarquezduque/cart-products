import React from 'react';


function Product( { product, add } ) {

    return (

      <div className = "col-sm pt-4">
        <div className = "card h-100" style = {{ width: '18rem' }}>
          <img className = "card-img-top" src = { product.imag } alt = { product.title }  /> 
          <div className = "card-body">
            <h5 className = "card-title">{ product.title }</h5>
            <p className="card-text" >${product.price}</p>
            <button onClick={(e) => add(product)} className="btn btn-success">+Add to Cart</button>
          </div>
        </div>
      </div>
    )
}

export default Product;
