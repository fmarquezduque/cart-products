import React from 'react';


function Cart({ state , add, remove  }) {

  
  return (

      <div className = "container" style = {{ paddingTop:'2rem', paddingBottom:'2rem' }}>
        <div>
          <div className = "text-center"> 
            <h5>Cart details</h5>
          </div>
          <div className = "row">
            <div className = "col">
              <ul className = "list-group" >
                  {
                    state.cart.map((product) => (
                      <li className = "list-group-item" key = { product.id }>
                        <div>
                          { product.title }
                        </div>
                        <div>
                          { product.quantity } * { product.price }
                        </div>
                         
                        { state.totalPrice >= 0 && <button className = "btn btn-danger" onClick={(e) => remove(product)}>-</button>}
                        <button className = "btn btn-primary" onClick={(e) => add(product)}>+</button> 
                      </li>
                    ))
                  }
              </ul>
            </div>
            <p>Price: ${ state.totalPrice } </p>

          </div>
        </div>
      </div>
  )
}

export default Cart;
