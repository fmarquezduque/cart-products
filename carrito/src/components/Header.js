import React from 'react';
import { Link } from "react-router-dom";

function Header( props ) {


  return (

    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
      <Link className="navbar-brand"  to  = "/">Unimag Products</Link>

      <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
        <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
          <li className="nav-item active">
            <Link className="nav-link" to = "/">Products</Link>
          </li>
        </ul>
        <form className="form-inline my-2 my-lg-0">
           <Link to = "/cart">
             {props.quantity ? (
               <button className = "btn btn-danger"> <i className="bi bi-cart3"></i>{'x'}{ props.quantity }</button>
             ) : (
              <button className = "btn btn-danger"> <i className="bi bi-cart3"></i></button>
             )}
           </Link>
        </form>
      </div>
    </nav>

  )
}

export default Header;
