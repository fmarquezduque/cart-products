

const Add = ( Itemproduct, product ) => {
    const exist = Itemproduct.find((x) => x.id === product.id);
    if (exist) {
      return  Itemproduct.map((x) => x.id === product.id ? { ...exist, quantity: exist.quantity + 1 } : x )
    } else {
      return [...Itemproduct, { ...product, quantity: 1 }];
    }
};


const Remove = (Itemproduct, product) => {
    const exist = Itemproduct.find((x) => x.id === product.id);
    if (exist.quantity === 1) {
      return Itemproduct.filter((x) => x.id !== product.id)
    } else {
        return Itemproduct.map((x) => x.id === product.id ? { ...exist, quantity: exist.quantity - 1 } : x)
    }
};

const reducer = ( state, action ) => {

      switch (action.type) {
        case 'ADD':
          return {
            ...state,
            quantity: state.quantity + 1,
            cart: Add(state.cart, action.payload),
            totalPrice: state.totalPrice + action.payload.price
          }
          break;
        case 'REMOVE':
          return {
            ...state,
            quantity: state.quantity - 1,
            cart: Remove(state.cart, action.payload),
            totalPrice: state.totalPrice - action.payload.price

          }
          break;
        default:
         return state;

      }

}

export default reducer;
