
import { useReducer } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import Products from './components/Products';
import Header from './components/Header';
import Cart from './components/Cart';
import datos from './products';
import reducer from './reducer';

const initialState = {
  quantity: 0,
  totalPrice: 0,
  cart : []
}


function App() {

  const { products } = datos;

  const [state, dispatch] = useReducer(reducer, initialState);

  const remove = ( product ) => dispatch({ type: "REMOVE", payload: product });
  const add = ( product ) => dispatch({ type: "ADD", payload: product });
  
  return (
    <Router>
      <Header quantity = {state.quantity}/>
      <Switch>
        <Route exact path="/">
          <Products products = { products } add = { add } />
        </Route>
        <Route exact path="/cart">
          <Cart state = { state } add = { add } remove = { remove } />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
