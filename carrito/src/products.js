const data = {

    products: [
      {
        'id':1,
        'imag':'./imgs/macBookPro2020.jpg',
        'title':'MacBook Pro',
        'price': 11000000
      },
      {
        'id':2,
        'imag':'./imgs/iPadPro2020.jpg',
        'title':'iPad Pro',
        'price': 9000000
      },
      {
        'id':3,
        'imag':'./imgs/iphone12.jpg',
        'title':'iPhone 12',
        'price': 7000000
      }
    ],
};

export default data;
